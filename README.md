##multiSelect


### Что это? ###

multiSelect - это дополнение к CRM-системе на базе [SugarCRM](http://http://www.sugarcrm.com/)/[SuiteCRM](https://suitecrm.com/), которое позволяет отображать выпадающие списки при поиске записей модулей в более компактном виде

### Установка ###

Модуль устанавливается через стандартный установщик модулей в CRM-системе. Более подробно в картинках на официальной [страничке проекта](http://spravkacrm.ru/files/file/4-multiselect-%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BA%D1%82%D0%BD%D1%8B%D0%B9-%D0%B2%D0%B8%D0%B4-%D1%81%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D1%87%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2/)
